import { TInputProps } from "components/Input/Input.type";
import { FC } from "react";
declare const Input: FC<TInputProps>;
export default Input;
