import { FC } from "react";
import { TButtonProps } from "components/Button/Button.type";
declare const Button: FC<TButtonProps>;
export default Button;
