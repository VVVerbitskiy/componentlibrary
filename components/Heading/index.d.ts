import { THeadingProps } from "components/Heading/Heading.type";
import { FC } from "react";
declare const Heading: FC<THeadingProps>;
export default Heading;
