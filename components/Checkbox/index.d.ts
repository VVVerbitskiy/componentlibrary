import { TCheckboxProps } from "components/Checkbox/Checkbox.type";
import { FC } from "react";
declare const Checkbox: FC<TCheckboxProps>;
export default Checkbox;
