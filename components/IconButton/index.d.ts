import { TIconButtonProps } from "components/IconButton/IconButton.type";
import { FC } from "react";
declare const IconButton: FC<TIconButtonProps>;
export default IconButton;
