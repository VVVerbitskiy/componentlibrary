import { TSelectProps } from "components/Select/Select.type";
import { FC } from "react";
declare const Select: FC<TSelectProps>;
export default Select;
