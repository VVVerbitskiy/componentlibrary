import { TPaginationProps } from "components/Pagination/Pagination.type";
import { FC } from "react";
declare const Pagination: FC<TPaginationProps>;
export default Pagination;
