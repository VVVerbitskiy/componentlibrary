import { TLabelProps } from "components/Label/Label.type";
import { FC } from "react";
declare const Label: FC<TLabelProps>;
export default Label;
