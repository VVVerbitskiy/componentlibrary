import { TNavbarItemProps } from "components/NavbarItem/NavbarItem.type";
import { FC } from "react";
declare const NavbarItem: FC<TNavbarItemProps>;
export default NavbarItem;
