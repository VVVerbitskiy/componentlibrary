import { TParagraphProps } from "components/Paragraph/Paragraph.type";
import { FC } from "react";
declare const Paragraph: FC<TParagraphProps>;
export default Paragraph;
