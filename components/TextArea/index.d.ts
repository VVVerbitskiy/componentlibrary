import { TTextAreaProps } from "components/TextArea/TextArea.type";
import { FC } from "react";
declare const TextArea: FC<TTextAreaProps>;
export default TextArea;
