import { TNavbarProps } from "components/Navbar/Navbar.type";
import { FC } from "react";
declare const Navbar: FC<TNavbarProps>;
export default Navbar;
