import { TModalProps } from "components/Modal/Modal.type";
import { FC } from "react";
declare const Modal: FC<TModalProps>;
export default Modal;
